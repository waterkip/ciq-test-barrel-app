// SPDX-FileCopyrightText: 2022 Wesley Schwengle <wesleys@opperschaap.net>
//
// SPDX-License-Identifier: BSD-3-Clause

import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;

class BarrelTestApp extends Application.AppBase {

    function initialize() {
        AppBase.initialize();
    }

    // onStart() is called on application start up
    function onStart(state as Dictionary?) as Void {
    }

    // onStop() is called when your application is exiting
    function onStop(state as Dictionary?) as Void {
    }

    // Return the initial view of your application here
    function getInitialView() as Array<Views or InputDelegates>? {
        return [ new AppProjectView(), new AppProjectDelegate() ] as Array<Views or InputDelegates>;
    }

}

function getApp() as BarrelTestApp {
    return Application.getApp() as BarrelTestApp;
}