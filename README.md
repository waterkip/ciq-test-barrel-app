CIQ Test Barrel App
==

Testing a barrel in a contained environment

To test your barrel we assume your project lives in `$HOME/code/ciq/MY-BARREL`
and this project lives in `$HOME/code/ciq/CIQ-Test-Barrel-App`.

Mount your barrel into the docker image:

```
# docker-compose.override.yml
    volumes:
      - ../MY-BARREL:/home/ciq/barrels/MY-BARREL
```

Add it to your monkey.jungle file per usual and in the manifest file per usual.
Now you have a barrel you can test :)
